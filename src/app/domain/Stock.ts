export class Stock {
  public symbol: string;
  public company: string;

  getPrice(): number {
    return 0;
  }

  getRoundedPrice(): number {
    return Math.round((Math.random() * 1000 * this.symbol.length) * 100 + Number.EPSILON) / 100;
  }
}
