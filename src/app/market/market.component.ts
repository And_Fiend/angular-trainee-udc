import {Component, OnInit} from '@angular/core';
import {Stock} from '../domain/Stock';
import {MarketService} from '../services/market.service';

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
})
export class MarketComponent implements OnInit {

  stocks: Stock[] = [];

  constructor(private marketService: MarketService) {
    this.stocks = [];
  }

  ngOnInit() {
    this.updateStocks();
  }

  updateStocks() {
    this.marketService.getStocks().subscribe(r => this.stocks = r);
  }

  add(symbol: string, company: string) {
    this.marketService.add(symbol, company);
    this.updateStocks();
  }
}
