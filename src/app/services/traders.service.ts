import {Injectable} from '@angular/core';
import {Trader} from '../domain/Trader';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TradersService {

  traders: Trader[] = [];

  constructor() {
    this.traders = this.getMockTraders();
  }


  getMockTraders(): Trader[] {
    const traders: Trader[] = []; // let

    traders.push(new Trader('Oleg'));
    traders.push(new Trader('Ana'));
    traders.push(new Trader('Igor'));
    traders.push(new Trader('Andrew'));
    traders.push(new Trader('Pasha'));
    traders.push(new Trader('Vova'));
    traders.push(new Trader('GG.bet'));

    return traders;
  }

  add(name: string) {
    this.traders.push(new Trader(name));
  }

  getTraders(): Observable<Trader[]> {
    return of<Trader[]>(this.traders);
  }

  // getTraders(): Promise<Trader[]> {
  //   return new Promise(resolve =>
  //     setTimeout(() => resolve(Promise.resolve(this.traders.slice())), 100));
  // } old unusable method

}
