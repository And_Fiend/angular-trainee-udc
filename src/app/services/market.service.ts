import {Injectable} from '@angular/core';
import {Stock} from '../domain/Stock';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MarketService {

  stocks: Stock[] = [];

  constructor(private httpClient: HttpClient) {
  }


  configUrl = 'assets/market-data.json';

  getStocks(): Observable<Stock[]> {
    return this.httpClient.get<Stock[]>(this.configUrl).pipe(
      map((array: Stock[]) => {
          return array.map((item: Stock) => {
            const stock = new Stock();
            stock.company = item.company;
            stock.symbol = item.symbol;
            return stock;
          });
        }
      ));
  }


  add(symbol: string, company: string) {
    this.stocks.push(new Stock(symbol, company));
  }

}

// export interface MarketService {
//   getPrice(symbol: string): number;
//   getUpdatedPrice(currentPrice: number): number;
//   getStocks(): Observable<Stock[]>;
//   addStock(symbol: string, company: string);
//
// }
