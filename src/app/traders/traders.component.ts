import {Component, OnInit} from '@angular/core';
import {Trader} from '../domain/Trader';
import {TradersService} from '../services/traders.service';

@Component({
  selector: 'app-traders',
  templateUrl: './traders.component.html',
  styleUrls: ['./traders.component.scss']
})
export class TradersComponent implements OnInit {

  traders: Trader[] = [];

  constructor(private tradersService: TradersService) {
  }

  ngOnInit() {
    this.traders = [];
    this.tradersService.getTraders().subscribe(result => this.traders = result);
  }


  add(name: string) {
    this.tradersService.add(name);
    this.updateTraders();
  }

  private updateTraders() {
    this.tradersService.getTraders().subscribe(result => this.traders = result);
  }

}
