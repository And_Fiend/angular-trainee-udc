import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TradersComponent} from './traders/traders.component';
import {MarketComponent} from './market/market.component';


const routes: Routes = [
  {
    path: '', redirectTo: '/market', pathMatch: 'full'
  },
  {
    path: 'traders', component: TradersComponent
  },
  {
    path: 'market',
    component: MarketComponent
  },
  {
    path: '**',
    component: MarketComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
